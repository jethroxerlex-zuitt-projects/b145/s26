//find operation
// db.collections.find({query},{field projection})
//query operators

//comparison Operators
//
db.users.find({"age:"{$lt:50}}) //less than 50

db.users.find({"age":{$gte:50}}) // greater than or equal to 50

db.users.find({"lastName":{$in:["Hawking","Doe"]}});

//Logical Query Operators

//"OR" logical operator
db.users.find({$or:[{"firstName":"Neil"},{"age":25}]});

db.users.find( { $and: [ { age: { $ne: 82 } }, { age: { $ne: 76 } } ] } );

//Field Projection

db.users.find(
	{
		"firstName":"Jane"
	},
	{
		"firstName": 1,
		"lastName": 1,
		"contact": 1
	}
	);

db.users.find(
	{
		"lastName":"Hawking"
	},
	{
		"contact": 0,
		"department": 0
	})

//look for a document/s that has a name neil and exclude id and department fields but include first & last name and contact


db.users.find(
	{
		"firstName":"Neil"
	},
	{
		"firstName": 1,
		"lastName": 1,
		"contact": 1,
		"_id": 0
	}
	);

//look for a document/s that has a name Bill and include first & last name and phone number

db.users.find(
	{
		"firstName":"Bill"
	},
	{
		"firstName": 1,
		"lastName": 1,
		"contact.phone": 1,
		"_id": 0
	}
	);